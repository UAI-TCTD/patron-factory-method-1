﻿Imports FactoryMethod

Module Module1

    Sub Main()
        Dim pizzeria As Pizzeria
        Dim pizza As Pizza
        pizzeria = New PizzeriaArgentina()

        pizza = pizzeria.CrearPizza("napo")
        pizza.Render()
        pizza = pizzeria.CrearPizza("cancha")
        pizza.Render()

        pizzeria = New PizzeriaItaliana()
        pizza = pizzeria.CrearPizza("napo")
        pizza.Render()
        pizza = pizzeria.CrearPizza("cancha")
        pizza.Render()
        Console.ReadKey()
    End Sub

End Module
