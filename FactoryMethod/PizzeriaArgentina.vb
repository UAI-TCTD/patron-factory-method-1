﻿Public Class PizzeriaArgentina
    Inherits Pizzeria


    Public Overrides Function CrearPizza(tipo As String) As Pizza
        If tipo = "cancha" Then
            Return New PizzaCancha("Argentina")
        ElseIf tipo = "napo" Then
            Return New PizzaNapolitana("Argentina")
        Else
            Return Nothing
        End If

    End Function
End Class
