﻿Public Class PizzeriaItaliana
    Inherits Pizzeria


    Public Overrides Function CrearPizza(tipo As String) As Pizza
        If tipo = "cancha" Then
            Return New PizzaCancha("Italia")
        ElseIf tipo = "napo" Then
            Return New PizzaNapolitana("Italia")
        Else
            Return Nothing
        End If

    End Function


End Class
